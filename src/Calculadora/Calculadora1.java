package Calculadora;

import java.util.Scanner;

public class Calculadora1 {

	static Scanner scn = new Scanner(System.in);
	
	static boolean acumulatiu = false;
	
	public static void main(String[] args) {
		
		int valor1=0;
		int valor2=0;
		int resultat = 0;
		boolean salir = false;
		
		do {
					
			switch(menu()) {
				case 1 : //Posar el valor
					System.out.println("Digues el valor1");
					valor1 = scn.nextInt();
					
					System.out.println("Digues el valor 2");
					valor2 = scn.nextInt();
					break;
					
				case 2: //Sumar
				resultat = sumar (valor1,valor2);
				System.out.println("El resultat es " +resultat);
					break;	
					
				case 3: //Restar
				resultat = restar (valor1,valor2);
				System.out.println("El resultat es " +resultat);
					break;
					
				case 4: //Multiplicar
				resultat = multiplicar (valor1,valor2);
				System.out.println("El resultat es " +resultat);
					break;
					
				case 5: //Dividir
				resultat = dividir (valor1,valor2);
				System.out.println("El resultat es " +resultat);
					break;
					
				case 0: //Salir
					salir = true;
			}
			
		}while (!salir);
		
	}
	public static int menu () {
	System.out.println("1.- Obtenir dos n�meros a operar");
	System.out.println("2.- Sumar ");
	System.out.println("3.- Restar");
	System.out.println("4.- Multiplicar");
	System.out.println("5.- Dividir");
	System.out.println("0.- Sortir del programa");	
	return scn.nextInt();
	}
	public static int sumar (int valor1, int valor2) {
		int resultat = valor1 + valor2;
		if (acumulatiu) {
			resultat = resultat + valor2;
		}
		else {
			resultat = valor1 + valor2;
		}
		acumulatiu = true;
		return resultat;
	}
	
	public static int restar (int valor1, int valor2) {
		int resultat = valor1 - valor2;
		if (acumulatiu) {
			resultat = resultat + valor2;
		}
		else {
			resultat = valor1 + valor2;
		}
		acumulatiu = true;
		return resultat;
	}
	
	public static int multiplicar (int valor1, int valor2) {
		int resultat = valor1 * valor2;
		if (acumulatiu) {
			resultat = resultat + valor2;
		}
		else {
			resultat = valor1 + valor2;
		}
		acumulatiu = true;
		return resultat;
	}
	
	public static int dividir (int valor1, int valor2) {
		int resultat = 0;
		if (valor2==0) {
			System.err.println("El numero no es pot dividir entre 0\n");	
		}
		else {
		resultat = valor1 % valor2;
		}
		return resultat;
	}
	
}

